<?php
   $title = "VideoLAN supports the UNHCR";
   $lang = "en";
   $menu = array( "vlc" );
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<h1>VideoLAN supports the UNHCR</h1>
<br />

<div class="longtext">
<div class="date">Paris, Oct 24 2022</div>

<p>VideoLAN is a non-profit organization which develops software for playing and processing multimedia content. It began as a student endeavor at École Centrale Paris (France) in 1996, turned into a multi-national open source project in 2001 and was constituted as a fully separate entity in 2008.</p>

<p>Our objective is to help and foster research, education and development of free and interoperable multimedia solutions. For more than twenty years, we welcome contributions from more than 1000 independent developers from more than 100 countries to fulfill our mission, on numerous pieces of software, including VLC.</p>

<p>As numerous other open-source organizations, we are socially aware and we care a lot about people and our users.<br />
However, we've always tried to limit our political positions to fights against digital rights management, software patents and for full interoperability. We do this to stay focused on our core mission.<br />
For other fights like Net neutrality, Free Data archiving, Equal access to development, we've always donated to other non-profits and kept those discussions outside of VideoLAN.</p>

<p>During the ongoing Syrian civil war for the last decade, we saw our software used for information purposes but also for distractions in challenging times. We also received major contributions to our project directly from contested areas resulting in full support for the Arabic language and making our software more accessible.</p>

<p>On February 24 this year, we saw a major disruption with Russia's invasion of Ukraine that likely resulted in tens of thousands of deaths and a permanent change to the political landscape. This led to unprecendented discussions of the VideoLAN board and across the non-profit, knowing that we have French, German and Russian citizens on the board, and we have members from numerous European countries including Ukrainian citizens.</p>

<p>We decided that our current contributions to information liberty were deficient to retain the current state of affairs and that we had to go beyond.</p>

<p>Seeing that in war, the people who suffer are always the normal people and the poor, seeing that open-source reaches everyone and everywhere, we decided to financially support the United Nations High Commissioner for Refugees (UNHCR) and their work on aiding and protecting forcibly displaced people and communities, in the places where they are necessary. We donated 10000€ of our treasury to UNHCR, which is a significant part of our yearly revenue.</p>

<p>VideoLAN is a de-facto pacifist organization and cares about cross-countries cooperations, and believes in the power of knowledge and sharing. War goes against those ideals.</p>

<h2>Press Contact</h2>
<p>press@videolan.org</p>

</div>

<?php footer('$Id: ios33.php 6098 2010-05-26 23:50:46Z jb $'); ?>
