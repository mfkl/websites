# Uzbek translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Hamza Foziljonov <hamza.foziljonov@gmail.com>, 2014-2015
# Shuhrat Dehkanov <k+transifex@efir.uz>, 2014
# Umid Almasov <u.almasov@gmail.com>, 2013
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2017-10-06 14:28+0200\n"
"Last-Translator: VideoLAN <videolan@videolan.org>, 2017\n"
"Language-Team: Uzbek (http://www.transifex.com/yaron/vlc-trans/language/"
"uz/)\n"
"Language: uz\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: include/header.php:289
msgid "a project and a"
msgstr ""

#: include/header.php:289
msgid "non-profit organization"
msgstr "notijorat tashkilot"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Hamkorlar"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr ""

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr ""

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Tadbirlar"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Huquqiy ma'lumot"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Matbuot markazi"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Biz bilan aloqa"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Yuklab olish"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Xususiyatlari"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr ""

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Yaxshi narsalardan oling"

#: include/menus.php:51
msgid "Projects"
msgstr "Loyihalar"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Barcha loyihalar"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Ishtirok etish"

#: include/menus.php:77
msgid "Getting started"
msgstr ""

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr ""

#: include/menus.php:79
msgid "Report a bug"
msgstr "Xato haqida xabar bering"

#: include/menus.php:83
msgid "Support"
msgstr "Yordam"

#: include/footer.php:33
msgid "Skins"
msgstr "Skinlar"

#: include/footer.php:34
msgid "Extensions"
msgstr "Kengaytmalar"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Skrinshotlar"

#: include/footer.php:61
msgid "Community"
msgstr "Uyushma"

#: include/footer.php:64
msgid "Forums"
msgstr "Forum"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Xabar tarqatish ro‘yxati"

#: include/footer.php:66
msgid "FAQ"
msgstr "TTSS"

#: include/footer.php:67
msgid "Donate money"
msgstr "Pulni taqdim etish"

#: include/footer.php:68
msgid "Donate time"
msgstr "Vaqtni taqdim etish"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Loyiha va tashkilot"

#: include/footer.php:76
msgid "Team"
msgstr "Jamoa"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Ko'zgular"

#: include/footer.php:83
msgid "Security center"
msgstr "Xavfsizlik markazi"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Ishtirok eting"

#: include/footer.php:85
msgid "News"
msgstr "Yangiliklar"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "VLC yuklab olish"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Boshqa tizimlar"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr ""

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC - bu bepul va ochiq kodli transtizim multimedia ijrochisi va freymvorki "
"bo'lib, aksariyat multimedia fayllar, DVD, audio CD, VCD hamda bir qancha "
"striming protokollar bilan ishlay oladi."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC: Rasmiy sayt - Hamma OT uchun erkin multimedia echimlari!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "VideoLANning boshqa loyihalari"

#: index.php:30
msgid "For Everyone"
msgstr "Hamma uchun"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC - bu aksariyat media kodek va formatlarni chala oladigan kuchli media "
"ijrochisidir."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Movie Creator - bu video fayllar yasash va taxrir qilish ilovasidir."

#: index.php:62
msgid "For Professionals"
msgstr "Professionallar uchun"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast - bu sodda, biroq kuchli MPEG-2/TS demultiplikatsiya va striming "
"qilish ilovasidir."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"Multicat - bu ko'pkanalli strimlarni oson va samaralli ravishda boshqarishga "
"mo'ljallangan ilovalar to'plamidir."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 - bu videolarni H.264/MPEG-4 AVC formatga kodlovchi bepul ilovadir."

#: index.php:104
msgid "For Developers"
msgstr "Tuzuvchilar uchun"

#: index.php:140
msgid "View All Projects"
msgstr "Hamma loyihalarni ko'rish"

#: index.php:144
msgid "Help us out!"
msgstr "Yordam bering!"

#: index.php:148
msgid "donate"
msgstr "xayriya qilish"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN bu notijorat tashkilot."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Barcha xarajatlarimiz foydalanuvchilar tomonidan qilingan xayriya "
"mablag‘lari evaziga qoplanadi. Agar siz ham VideoLAN dasturini yoqtirsangiz, "
"iltimos, bizni qo‘llab-quvvatlash uchun xayriya qiling."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "O'rganing"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN bu ochiq kodli dasturiy ta'minot."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Bu shuni anglatadiki, agar sizda ham biror mahsulotimizni yaxshilashga oid "
"tajriba va xohish bo‘lsa, marhamat, bizga qo‘shiling."

#: index.php:187
msgid "Spread the Word"
msgstr "Xabar tarqatish"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Bizning fikrimizcha, VideoLAN eng yaxshi narxda, ya’ni bepul tarqatiluvchi "
"eng yaxshi video pleyerdir. Agar siz ham bu fikrga qo‘shilsangiz, iltimos "
"ushbu dastur haqida do‘stlaringizga ham xabar qiling."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Yangiliklar"

#: index.php:218
msgid "More News"
msgstr ""

#: index.php:222
msgid "Development Blogs"
msgstr "Tuzuvchilar blogi"

#: index.php:251
msgid "Social media"
msgstr "Ijtimoiy tarmoqlar"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "VLC olish"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr ""

#: vlc/index.php:35
msgid "Plays everything"
msgstr ""

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr ""

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr ""

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr ""

#: vlc/index.php:44
msgid "Completely Free"
msgstr ""

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr ""

#: vlc/index.php:47
msgid "learn more"
msgstr ""

#: vlc/index.php:66
msgid "Add"
msgstr ""

#: vlc/index.php:66
msgid "skins"
msgstr ""

#: vlc/index.php:69
msgid "Create skins with"
msgstr ""

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr ""

#: vlc/index.php:72
msgid "Install"
msgstr ""

#: vlc/index.php:72
msgid "extensions"
msgstr ""

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Hamma rasmlarini ko'rish"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "VLC media pleyerning rasmiy yuklashlar"

#: vlc/index.php:146
msgid "Sources"
msgstr "Manba kodlari"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Bevosita olishingiz mumkin:"

#: vlc/index.php:148
msgid "source code"
msgstr "manba kodi"

#~ msgid "A project and a"
#~ msgstr "Loyiha va"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "erkin va ochiq kodli multimedia yechimlarini ishlab chiqadigan hamda "
#~ "qo‘llab-quvvatlaydigan ko‘ngillilardan tashkil topgan."

#~ msgid "why?"
#~ msgstr "nega?"

#~ msgid "Home"
#~ msgstr "Boshi"

#~ msgid "Support center"
#~ msgstr "Yordam markazi"

#~ msgid "Dev' Zone"
#~ msgstr "Developer hududi"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Oddiy, tez va kuchli media pleyer."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr ""
#~ "Hamma narsani ijro etadi: fayllar, disklar, veb kameralar, uskunalar va "
#~ "oqimlarni."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "Ko'p kodeklarni ijro etadi kodek-paklarning hojati yo'q:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Hamma platformalarda ishlaydi:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "Butunlay bepul hamda josuslik, reklama va foydalanuvchini kuzatuvchi "
#~ "dasturlarsiz."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Media konvertlash va oqimli video imkoniyatlariga ega."

#~ msgid "Discover all features"
#~ msgstr "Hamma hususiyatlarini kashf eting"
