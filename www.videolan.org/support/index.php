<?php
   $title = "User support and help";
   $lang = "en";
   $body_color = "red";
   $new_design = true;
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<div class="container">

    <div class="row">
        <div class="col-md-8">
            <h1 class="bigtitle">Support and Help center</h1>

            <p>VideoLAN is free and open source software; and is not backed by any company.
            Developers are mostly <strong>volunteers</strong>.<br />
            Therefore, <strong>please remember</strong> that every user support
            is provided by <strong>volunteers</strong> doing it in their free time.<br />
            Noone owes you an answer to your question.</p>

            <h2 style="margin-top: 30px;">You are here for:</h2>
            <ul class="list-inline">
                <li style="margin-top: 1em;">
                    <a class="btn btn-default btn-lg" href="#documentation">Documentation</a>
                </li>
                <li style="margin-top: 1em;">
                    <a class="btn btn-default btn-lg" href="#help">Help and support</a>
                </li>
                <li style="margin-top: 1em;">
                    <a class="btn btn-default btn-lg" href="#bugs">Reporting a bug</a>
                </li>
            </ul>
        </div>
        <div class="col-md-4">
            <?php include($_SERVER["DOCUMENT_ROOT"]."/include/3.0.12-update-card.php"); ?>
        </div>
    </div>

    <hr />
    <h1 id="documentation">Documentation</h1>

    <h3>FAQ</h3>
    <p>If you have any questions about VideoLAN or about VLC media player, you should
    <strong>always start</strong> by reading the <a href="faq.html">Official FAQ</a>.</p>

    <h3>Documentation</h3>
    <p>
      You can access our <a href="https://docs.videolan.me/">Documentation</a>,
      especially the <a href="https://docs.videolan.me/vlc-user/">VLC User Documentation</a>.
    </p>
    <p>You can also check our
      <a href="https://wiki.videolan.org/Documentation">Good old community-driven knowledge base</a>.
    </p>

    <hr />
    <h1 id="help">Help and Support</h1>
    <div class="row">
        <div class="col-md-6">
            <h3>You have a Usage Problem</h3>
            <p>If you have problems using our software, you should:</p>
            <ul class="bullets">
                <li>Search the <a href="https://docs.videolan.me/vlc-user/en/index.html">
                              FAQ & Support page of the VLC User Documentation
                            </a>
                </li>
                <li>Use the <a href="https://wiki.videolan.org/Common_Problems">
                              Community-driven troubleshooting guide
                            </a>
                </li>
                <li>
                    <a href="https://forum.videolan.org/search.php">Search</a> the official
                    <a href="https://forum.videolan.org/">forum</a> for an answer
                </li>
                <li>Read the <a href="https://wiki.videolan.org">Wiki</a></li>
            </ul>
        </div>

        <div class="col-md-6">
            <h3>You want to ask a question</h3>
            <p>
                To ask a question, you <strong>need</strong> to prepare a <strong>correct report</strong> using the <a href="https://wiki.videolan.org/VLC_report">VLC report page</a>.
            </p>
            <p>Then ask your question, on:</p>
            <ul class="bullets">
                <li>Our <a href="https://forum.videolan.org">Forum</a>, in the correct section</li>
                <li>
                    Our live-chat IRC channel on the <a href="http://libera.chat">Libera.chat</a> Network:<br />
                    Server: <code>irc.libera.chat</code></br>
                    Channel: <code>#videolan</code></br>
                    Use the <a href="https://kiwiirc.com/nextclient/#ircs://irc.libera.chat/#videolan">KiwiIRC Web</a> interface, if you don't have an IRC client at hand.
                </li>
            </ul>
        </div>
    </div>

    <hr />
    <div class="row">
        <div class="col-md-4">
            <h1 id="bugs">Reporting bugs <br/><small>and feature requests</small></h1>
            <p>Please read our <a href="https://wiki.videolan.org/Report_bugs">reporting bugs policy</a>.</p>

            <h3>VLC bug tracker</h3>
            <p>Go to the <a href="https://code.videolan.org/videolan/vlc/-/issues/"><b>VLC</b> bug reporting site</a>.</p>

            <h3>VLMC bug tracker</h3>
            <p>Go to the <a href="https://code.videolan.org/videolan/vlmc/-/issues/"><b>VLMC</b> bug reporting site</a>.</p>
        </div>

        <div class="col-md-4">
            <h1>Consulting services</h1>
            <p>
                If you are a professionnal, and need services around our products, some companies provide professionnal support. They are listed on the <a href="/vlc/partners.html">consultants</a> page.
            </p>
        </div>

        <div class="col-md-4">
            <h1>Contacting the team</h1>
            <p>For other matters, see the <a href="/contact.html">contact page</a>.</p>
        </div>
    </div>

</div>
<?php footer('$Id$'); ?>
