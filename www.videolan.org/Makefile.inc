# Subdirectories to be preprocessed
SUBDIR = doc security support vlc videolan projects developers locale press vlmc

# Files to be preprocessed
#PHP_FILES = # automatic
PHP_IGNORES =

# Additionnal dependencies

news.html: news.msg

news-rss.html: news.msg

index.html: news.msg

style/style.css: style/style.scss style/theme.scss style/mixins.scss
	@echo "Processing $< ..."
	sassc $< > $@ || sass $< > $@

style/style.min.css: style/style.css
	@echo "Processing $< ..."
	yui-compressor --type css $< > $@.tmp || cp $< $@.tmp
	rm -f $@ && mv $@.tmp $@

style/bootstrap.css: style/bootstrap.scss
	@echo "Processing $< ..."
	sassc $< > $@ || sass $< > $@

style/bootstrap.min.css: style/bootstrap.css
	@echo "Processing $< ..."
	yui-compressor --type css $< > $@.tmp || cp $< $@.tmp
	rm -f $@ && mv $@.tmp $@

style/slick.css: style/slick.scss style/slick-theme.scss
	@echo "Processing $< ..."
	sassc $< > $@ || sass $< > $@

style/slick.min.css: style/slick.css
	@echo "Processing $< ..."
	yui-compressor --type css $< > $@.tmp || cp $< $@.tmp
	rm -f $@ && mv $@.tmp $@

js/slick.min.js: js/slick.js
	@echo "Processing $< ..."
	yui-compressor --type js $< > $@.tmp || cp $< $@.tmp
	rm -f $@ && mv $@.tmp $@

locales: $(addsuffix .php,$(addprefix index.,$(shell find locale -maxdepth 1 -mindepth 1 -type d | sed 's,locale/,,'))) include/header.php
