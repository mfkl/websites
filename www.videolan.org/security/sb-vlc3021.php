<?php
   $title = "VideoLAN Security Bulletin VLC 3.0.21";
   $lang = "en";
   $menu = array( "vlc" );
   $body_color = "red";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>


<div id="fullwidth">

<h1>Security Bulletin VLC 3.0.21</h1>
<pre>
Summary           : Vulnerability fixed in VLC media player
Date              : June 2024
Affected versions : VLC media player 3.0.20 and earlier
ID                : VideoLAN-SB-VLC-3021
</pre>

<h2>Details</h2>
<p>A denial of service through a potential integer overflow could be triggered with a maliciously crafted mms stream (heap based overflow)</p>

<h2>Impact</h2>
<p>If successful, a malicious third party could trigger either a crash of VLC or an arbitratry code execution with the privileges of the target user.</p>
<p>While these issues in themselves are most likely to just crash the player, we can't exclude that they could be combined to leak user informations or remotely execute code. ASLR and DEP help reduce the likelyness of code execution, but may be bypassed.</p>
<p>We have not seen exploits performing code execution through this vulnerability.</p>
<br />

<h2>Threat mitigation</h2>
<p>Exploitation of those issues requires the user to explicitly open a maliciously crafted mms stream.</p>

<h2>Workarounds</h2>
<p>The user should refrain from opening mms streams from untrusted third parties (or disable the VLC browser plugins), until the patch is applied.</p>

<h2>Solution</h2>
<p>VLC media player <b>3.0.21</b> addresses the issue.</p>

<h2>Credits</h2>
<p>Reported by Andreas Fobian of Mantodea Security GmbH</p>

<h2>References</h2>
<dl>
<dt>The VideoLAN project</dt>
<dd><a href="//www.videolan.org/">http://www.videolan.org/</a>
</dd>
<dt>VLC official GIT repository</dt>
<dd><a href="https://code.videolan.org/videolan/vlc.git">https://code.videolan.org/videolan/vlc.git</a>
</dd>
</dl>

</div>

<?php footer('$Id$'); ?>
