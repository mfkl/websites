<?php
   $title = "VideoLAN Security Bulletin VLC 3.0.19";
   $lang = "en";
   $menu = array( "vlc" );
   $body_color = "red";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>


<div id="fullwidth">

<h1>Security Bulletin VLC 3.0.19</h1>
<pre>
Summary           : Two vulnerabilities fixed in VLC media player
Date              : November 2023
Affected versions : VLC media player 3.0.18 and earlier
ID                : VideoLAN-SB-VLC-3019
</pre>

<h2>Details</h2>
<p>Fix potential arbitrary code execution with system priviledges on uninstallation on Windows (!4292, CVE-2023-46814)</p>

<h2>Impact</h2>
<p>If successful, a malicious third party could trigger an execution of an arbitrary binary on uninstallation of VLC with system priviledges.</p>
<p>We have not seen exploits performing code execution through this vulnerability.</p>
<br />

<h2>Threat mitigation</h2>
<p>Exploitation of this issue requires the user to explicitly uninstall VLC using the provided uninstaller.</p>

<h2>Workarounds</h2>
<p>Keep VLC installed until updated to version 3.0.19 or later.</p>

<h2>Solution</h2>
<p>VLC media player <b>3.0.19</b> addresses the issue.</p>

<h2>Credits</h2>
<p>The NSIS uninstaller vulnerability was reported by the Lockheed Martin Red Team (!4292, CVE-2023-46814).</p>

<h2>Additional notes</h2>
<p>VLC 3.0.19 also bumps some dependencies, notably zlib and vpx, following the publication of CVE-2022-37434 and CVE-2023-5217.</p>

<h2>References</h2>
<dl>
<dt>The VideoLAN project</dt>
<dd><a href="//www.videolan.org/">http://www.videolan.org/</a>
</dd>
<dt>VLC official GIT repository</dt>
<dd><a href="http://git.videolan.org/?p=vlc/vlc-3.0.git">http://git.videolan.org/?p=vlc.git</a>
</dd>
</dl>

</div>

<?php footer('$Id$'); ?>
