<?php
   $title = "VideoLAN Security Bulletin VLC-iOS 3.5.9";
   $lang = "en";
   $menu = array( "vlc" );
   $body_color = "red";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>


<div id="fullwidth">

<h1>Security Bulletin VLC-iOS 3.5.9</h1>
<pre>
Summary           : Vulnerability fixed in VLC media player
Date              : June 2024
Affected versions : VLC-iOS 3.5.7 and earlier
ID                : VideoLAN-SB-VLC-iOS-359
</pre>

<h2>Details</h2>
<p>A potential path traversal via the included WiFi File Sharing feature could be used for arbitrary data uploads by malicious parties on the local network to storage locations invisible to the user within the application context.</p>

<h2>Impact</h2>
<p>If successful, a malicious third party could trigger a denial-of-service of the device to due exceeeded storage space or implications of existance of arbitrary data. No read access was possible to third party. No write access outside the application container was possible.</p>
<p>We have not seen exploits through this vulnerability.</p>
<p>The tvOS port of the app was not affected.</p>
<br />

<h2>Threat mitigation</h2>
<p>Exploitation of this issue requires the user to explicitly start WiFi File Sharing on a local network with potential malicious actors.</p>

<h2>Workarounds</h2>
<p>The user should refrain from enabling WiFi File sharing on local networks with potential malicious actors until the update is installed.</p>

<h2>Solution</h2>
<p>VLC-iOS <b>3.5.9</b> addresses the issue.</p>

<h2>Credit</h2>
<p>Reported by Allar Lauk of TalTech University (Estonia)</p>

<h2>References</h2>
<dl>
<dt>The VideoLAN project</dt>
<dd><a href="//www.videolan.org/">http://www.videolan.org/</a>
</dd>
<dt>VLC-iOS GIT repository</dt>
<dd><a href="https://code.videolan.org/videolan/vlc-ios.git">https://code.videolan.org/videolan/vlc-ios.git</a>
</dd>
</dl>

</div>

<?php footer('$Id$'); ?>
