<?php
   $title = "13th Video Dev Days, November 2nd-3rd, 2024";
   $body_color = "orange";

   $new_design = true;
   $show_vdd_banner = false;
   $lang = "en";
   $menu = array( "videolan", "events" );

   $rev = time();

   $additional_js = array("/js/slimbox2.js", "/js/slick-init.js", "/js/slick.min.js");
   $additional_css = array("/js/css/slimbox2.css", "/style/slick.min.css", "/videolan/events/vdd24/style.css?$rev");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

  <div class="sponsor-box-2 d-xs-none d-md-block">
    <h4>Sponsors</h4>
    <a href="https://videolabs.io/" target="_blank">
        <?php image( 'events/vdd19/sponsors/videolabs.png' , 'Videolabs', 'sponsors-logo'); ?>
    </a>
  </div>
  <header class="header-bg">
      <div class="container">
        <div class="row col-lg-10 col-lg-offset-1">
            <!--img class="img-fluid center-block" style="max-width:100%; height:auto; border-radius: 4%;" src="/images/events/vdd24/vdd24.png" alt="vdd logo"-->
	  <div class="col-sm-12 col-md-12 text-center align-middle" style="background: url('/images/events/vdd24/vdd24wide.png') no-repeat; background-size: contain; background-position:center; min-height:400px">
            <h1 id="videodevdays">Video Dev Days 2024</h1>
            <h3>The Open Multimedia Conference - Welcome to Cône-re-a !</h3>
            <h4>2nd - 3rd November, 2024, Seoul</h4>
          </div>
        </div>
      </div>
   </header>

<section id="overview">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 text-center">
        <h2 class="uppercase">
			 About
          <span class="spacer-inline"></span>
        </h2>

<p><a href="/videolan/">The VideoLAN non-profit organization</a> is excited to invite you to the upcoming multimedia open-source event in Seoul !</p>

<p>In this edition, participants from the VideoLAN and open-source multimedia communities will gather in <strong>Seoul</strong> to discuss and collaborate on advancing the future of open-source multimedia.</p>

<p>This technical conference will focus on in-depth multimedia topics, including codecs and their implementations such as <a href="https://www.videolan.org/developers/x264.html">x264</a> and <a href="https://code.videolan.org/videolan/dav1d">dav1d</a>, as well as frameworks like FFmpeg, and projects like <strong>VideoLAN</strong>, <strong>VLC</strong>, <strong>Kodi</strong>, <strong>libplacebo</strong>, <strong>mpv</strong> and related projects.</p>

<p>This event is specifically geared towards the technical aspects of multimedia, emphasizing low-level components like codecs, their implementations, frameworks, playback libraries, and various innovative projects within the multimedia domain.</p>

		<div>
		  <div class="row">
          <div class="col-md-6">
            <div class="text-box" id="when-box">
              <h4 class="text-box-title">When ?</h4>
              <p class="text-box-content">2 - 3 November 2024</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box" id="where-box">
              <h4 class="text-box-title">Where ?</h4>
              <p class="text-box-content">Seoul, South Korea</p>
            </div>
          </div>
        </div>

		  <div class="row">
          <div class="col-md-6">
            <div class="text-box" id="who-come-box">
              <h4 class="text-box-title">For ?</h4>
              <p class="text-box-content">
        				<strong>Anyone</strong> who cares about open source multimedia technologies and development.
				  </p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box" id="partners-box">
              <h4 class="text-box-title">From ?</h4>
              <p class="text-box-content">VideoLAN. But you are very welcome to co-sponsor the event.</p>
            </div>
          </div>
        </div>
 		
		</div>

      </div>
    </div>
  </div>
</section>

<section id="registration">
  <div class="container">
    <h2 class="text-center uppercase">Registration</h2>
    <hr class="spacer-2">
    <div class="row text-center">
      <div class="col">
	Register for free at <a href="https://framaforms.org/vdd-2024-1723106137" target="_new">VDD 24 registration form</a>
      </div>
    </div>
  </div>
</section>

<section id="schedule">
  <div class="container">
    <div class="row">
      <div class="text-center">
        <h2 class="uppercase">Schedule</h2>
        <hr class="spacer-2">
      </div>
      <div class="col-lg-10 col-lg-offset-1">
      <!-- Nav tabs -->

      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="#friday" aria-controls="friday" role="tab" data-toggle="tab">Friday 1</a></li>
        <li role="presentation" class="active"><a href="#saturday" aria-controls="saturday" role="tab" data-toggle="tab">Saturday 2</a></li>
        <li role="presentation"><a href="#sunday" aria-controls="sunday" role="tab" data-toggle="tab">Sunday 3</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade" id="friday">
            <div class="event">
              <h4 class="event-time">09:00</h4>
              <div class="event-description">
                <h3>Community Bonding Day</h3>
                <p>
                </p>
              </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade active in" id="saturday">
        </div>

        <div role="tabpanel" class="tab-pane fade" id="sunday">
        </div>
        
      </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Sponsors</h2>
    <hr class="spacer-2">

    <section class="text-center">
     <div class="row col-xs-10 col-xs-offset-1">
	     <div class="col col-md-6 col-sm-12">
        	<a href="https://videolabs.io/" target="_blank">
	          <?php image( 'events/vdd19/sponsors/videolabs.png' , 'Videolabs', 'sponsors-logo-2'); ?>
	        </a>
	      </div>
     </div>
    </section>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Access</h2>
    <hr class="spacer-2">
    <div class="row">
      <div class="col-md-6">
      	<h3 class="text-center uppercase">Conference</h3>
			<p>Kwangwoon University</p>
                        <p>TBA</p>
                        <iframe width="75%" height="200" src="https://www.openstreetmap.org/export/embed.html?bbox=127.05416500568391%2C37.6191222676269%2C127.05867111682893%2C37.6221093110245&amp;layer=mapnik&amp;marker=37.620615804327485%2C127.05641806125641" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=37.62062&amp;mlon=127.05642#map=18/37.62062/127.05642">Open map in full</a></small>

      </div>
      <div class="col-md-6">
      	<h3 class="text-center uppercase">Hotel</h3>
			<p>Sponsorised accomodation will be listed here</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
      	<h3 class="text-center uppercase">Beer Event</h3>
			<p>There might be beer in Korea as well</p>
      <div class="col-md-6">
     </div>
    </div>
  </div>
</section>


<section>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="uppercase">Code of Conduct </h2>
        <p>This community activity is running under the <a href="https://wiki.videolan.org/CoC/">VideoLAN Code of Conduct</a>. We expect all attendees to respect our <a href="https://wiki.videolan.org/VideoLAN_Values/">Shared Values</a>.</p>
      </div>
      <div class="col-md-6">
        <h2 class="uppercase">Contact </h2>
        <p>The VideoLAN Dev Days are organized by the board members of the VideoLAN non-profit organization, Jean-Baptiste Kempf, Denis Charmet, Felix Paul Khüne and Konstantin Pavlov. You can reach us at <span style="color: #39b549">board@videolan.org</span>.</p>
      </div>
    </div>
  </div>
</section>

<?php footer('$Id: index.php 5400 2009-07-19 15:37:21Z jb $'); ?>
